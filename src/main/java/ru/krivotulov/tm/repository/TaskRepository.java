package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> taskList = new ArrayList<>();

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task add(final Task task) {
        taskList.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return taskList;
    }

    @Override
    public List<Task> findAll(Comparator comparator) {
        final List<Task> result = new ArrayList<>(taskList);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : taskList) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : taskList) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index > taskList.size() || taskList.isEmpty()) return null;
        return taskList.get(index);
    }

    @Override
    public Task delete(final Task task) {
        taskList.remove(task);
        return task;
    }

    @Override
    public Task deleteById(String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        taskList.remove(task);
        return task;
    }

    @Override
    public Task deleteByIndex(Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        taskList.remove(task);
        return task;
    }

    @Override
    public void clear() {
        taskList.clear();
    }

}
