package ru.krivotulov.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public interface TerminalUtil {

    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    static Date nextDate() {
        try {
            String value = bufferedReader.readLine();
            return DateUtil.toDate(value);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    static String readLine() {
        try {
            return bufferedReader.readLine();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    static Integer nextNumber() {
        try {
            String value = bufferedReader.readLine();
            return Integer.parseInt(value);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

}
