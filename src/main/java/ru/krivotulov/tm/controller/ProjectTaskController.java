package ru.krivotulov.tm.controller;

import ru.krivotulov.tm.api.controller.IProjectTaskController;
import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.readLine();
        System.out.println("TASK ID: ");
        final String taskId = TerminalUtil.readLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("TASK ID: ");
        final String taskId = TerminalUtil.readLine();
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.readLine();
        projectTaskService.unbindTaskFromProject(taskId, projectId);
    }

}
