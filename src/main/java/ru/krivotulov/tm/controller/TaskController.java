package ru.krivotulov.tm.controller;

import ru.krivotulov.tm.api.controller.ITaskController;
import ru.krivotulov.tm.api.service.ITaskService;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.DateUtil;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    private void renderTasks(final List<Task> taskList) {
        int index = 1;
        for (Task task : taskList) {
            if (task == null) continue;
            System.out.println(index + "." + task);
            index++;
        }
    }

    @Override
    public void displayTaskList() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.readLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> taskList = taskService.findAll(sort);
        renderTasks(taskList);
        System.out.println("[OK]");
    }

    @Override
    public void displayTaskListByProject() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.readLine();
        final List<Task> taskList = taskService.findAllByProjectId(projectId);
        renderTasks(taskList);
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        System.out.println("ENTER DATE BEGIN: ");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END: ");
        final Date dateEnd = TerminalUtil.nextDate();
        final Task task = taskService.create(name, description, dateBegin, dateEnd);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.deleteByIndex(index);
        if (task == null) System.out.println("FAIL");
        else System.out.println("OK");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final Task task = taskService.deleteById(id);
        if (task == null) System.out.println("FAIL");
        else System.out.println("OK");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        showTask(task);
        System.out.println("OK");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("FAIL");
            return;
        }
        showTask(task);
        System.out.println("OK");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("OK");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    private void showTask(Task task) {
        if (task == null) return;
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("ID: " + task.getId());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
        final Status status = task.getStatus();
        if (status != null) System.out.println("STATUS: " + status.getDisplayName());
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.readLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.readLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeStatusById(id, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final Task task = taskService.changeStatusById(id, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeStatusByIndex(index, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final Task task = taskService.changeStatusById(id, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
