package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.repository.ICommandRepository;
import ru.krivotulov.tm.api.service.ICommandService;
import ru.krivotulov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
