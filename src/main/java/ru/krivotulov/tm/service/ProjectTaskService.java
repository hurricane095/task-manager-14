package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository,
                              final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(String projectId, String taskId) {
        if(projectId == null || projectId.isEmpty()) return;
        if(taskId == null || taskId.isEmpty()) return;
        if(!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if(task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(String taskId, String projectId) {
        if(projectId == null || projectId.isEmpty()) return;
        if(taskId == null || taskId.isEmpty()) return;
        if(!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if(task == null) return;
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(String projectId) {
        if(projectId == null || projectId.isEmpty()) return;
        final List<Task> taskList = taskRepository.findAllByProjectId(projectId);
        for(final Task task: taskList) taskRepository.deleteById(task.getId());
        projectRepository.deleteById(projectId);
    }

}
