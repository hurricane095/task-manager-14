package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService {

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAll(Sort sort);

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task delete(Task task);

    Task deleteById(String id);

    Task deleteByIndex(Integer index);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    void clear();

}
