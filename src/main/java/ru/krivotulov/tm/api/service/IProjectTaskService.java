package ru.krivotulov.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskFromProject(String taskId, String projectId);

    void removeProjectById(String projectId);

}
